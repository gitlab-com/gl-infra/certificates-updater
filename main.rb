require 'json'
require 'openssl'
require 'net/http'
require 'tmpdir'
require 'set'
require 'pathname'

require_relative 'lib/logging.rb'
require_relative 'lib/certificate.rb'
require_relative 'lib/environment.rb'
require_relative 'lib/helpers.rb'
require_relative 'lib/secrets.rb'
require_relative 'lib/sslmate.rb'

require_relative 'lib/secret_sources/gkms.rb'
require_relative 'lib/secret_sources/hashicorp_vault.rb'

at_exit do
  unless $!.nil? || ($!.is_a?(SystemExit) && $!.success?)
    msg = <<-END.gsub(/^\s+\|/, '')
      |There was an error running the certificates updater to renew certificates. Certificates may expire unless this is fixed.
      |
      |See #{ENV['CI_JOB_URL']} for more info (<#{ENV['CI_PROJECT_URL']}|repo>).
    END
    send_slack_msg(msg)
  end
end

changes = {}

environments = Environment.all
secret_sources = (ENV['SECRET_SOURCES'] || 'gkms,hashicorp_vault').split(',')

if (environments_list = ENV['ENVIRONMENTS'])
  targeted_environments = Set.new(environments_list.split(','))
  environments.select! { |environment| targeted_environments.include?(environment.name.to_s) }
end

if environments.empty?
  Logging.error "No environments found!"
  exit 1
end

other_vault_paths = (ENV['OTHER_VAULT_PATHS'] || '').split(',')

dry_run = !ENV['DRY_RUN'].nil? && !ENV['DRY_RUN'].empty?

Logging.logger.level = ENV['LOG_LEVEL'] ? Logger.const_get(ENV['LOG_LEVEL'].upcase) : Logger::INFO

Logging.debug "Running in dry-run mode" if dry_run
Logging.debug "Target environments: #{environments.map { |x| x.name }.join(', ')}"
Logging.debug "Secret sources: #{secret_sources.join(', ')}"

Logging.info "Now updating GKMS and Chef Vault secrets per environment"
environments.each do |environment|
  Logging.info "=> ENVIRONMENT: #{environment.name}", true

  if secret_sources.include?('gkms')
    Logging.info "==> Secrets source: GKMS"
    Dir.mktmpdir do |dir|
      environment.download_vaults(to: dir)

      Dir["#{dir}/**/*.enc"].each do |vault_path|
        relative_vault_path = vault_path.sub(dir, '')
        Logging.debug "Processing #{relative_vault_path} ...", true

        gkms    = GKMS.new(environment, vault_path)
        secrets = Secrets.new(gkms.decrypt, relative_vault_path)

        updated_certs = secrets.update_certificates
        if updated_certs.any?
          changes[environment] ||= {}
          changes[environment][relative_vault_path] = updated_certs
        else
          Logging.debug "No updates were made for #{relative_vault_path}"
          next
        end

        gkms.encrypt(JSON.pretty_generate(secrets.content))
      end

      if changes[environment]
        environment.upload_vaults(from: dir) unless dry_run
      else
        Logging.info "No updates were made for the '#{environment.name}' environment"
      end
    end
  end

  if secret_sources.include?('hashicorp_vault')
    Logging.info "==> Secrets source: Hashicorp Vault"
    hc_vault = HashicorpVault.new()
    hc_vault.mount = "chef"
    hc_vault.environment = environment
    hc_vault.get_secrets_list(environment.prefix)
    if hc_vault.secrets_with_certs
      Logging.debug "Found the following secrets with certs: #{hc_vault.secrets_with_certs.keys.join(', ')}"
      hc_vault.secrets_with_certs.each { |path, payload|
        Logging.debug "Processing #{path} ...", true

        secrets = Secrets.new(payload, path)
        updated_certs = secrets.update_certificates

        if updated_certs.any?
          hc_vault.update(path, secrets.content) unless dry_run

          changes[environment] ||= {}
          changes[environment][path] = updated_certs
        end
      }
    end

    unless changes[environment]
      Logging.info "No updates were made for the '#{environment.name}' environment"
    end
  end
end

if secret_sources.include?('hashicorp_vault') && !other_vault_paths.empty?
  Logging.info "=> Now updating secrets under other Vault paths"
  other_vault_paths.each { |path|
    full_path = Pathname(path).each_filename.to_a
    mount = full_path.shift
    hc_vault = HashicorpVault.new()
    hc_vault.mount = mount
    hc_vault.get_secrets_list(full_path.join("/"))
    if hc_vault.secrets_with_certs
      Logging.debug "Found the following secrets with certs: #{hc_vault.secrets_with_certs.keys.join(', ')}"
      hc_vault.secrets_with_certs.each do |path, payload|
        Logging.debug "Processing #{path} ...", true
  
        secrets = Secrets.new(payload, path)
        updated_certs = secrets.update_certificates
  
        if updated_certs.any?
          hc_vault.update(path, secrets.content) unless dry_run
  
          changes[path] = updated_certs
        end
      end
    end
    unless changes[path]
      Logging.info "No updates were made to secrets under Vault path #{path}"
    end
  }
end

notify_via_slack(changes)
