require 'vault'

class HashicorpVault
  attr_reader :secrets_with_certs
  attr_accessor :environment
  attr_reader :mount

  def initialize
    @secrets_with_certs = {}

    Vault.address = get_required_env('VAULT_ADDR')

    vault_token = ENV['VAULT_TOKEN']
    unless vault_token
      vault_auth_path = get_required_env('VAULT_AUTH_PATH')
      vault_auth_role = get_required_env('VAULT_AUTH_ROLE')
      vault_id_token = get_required_env('VAULT_ID_TOKEN')

      auth_secret = Vault.client.logical.write("auth/#{vault_auth_path}/login", role: vault_auth_role, jwt: vault_id_token)
      vault_token = auth_secret.auth.client_token
    end
    Vault.token = vault_token
  end

  def mount=(mountpath)
    @mount = mountpath
    @kv = Vault.kv(@mount)
  end

  def get_secrets_list(path = "")
    path.sub!(/\/$/, '')
    list = @kv.list(path)
    Logging.debug "Found the following secrets at '#{@mount}/#{path}': #{list}"
    list.each do |item|
      Logging.debug "Processing: '#{@mount}/#{path}/#{item}"
      if item.end_with?("/")
        self.get_secrets_list("#{path}/#{item}")
      else
        if item != ".placeholder"
          secret = @kv.read("#{path}/#{item}")
          @secrets_with_certs["#{path}/#{item}"] = secret.data if secret&.data.to_s.include?('-----BEGIN CERTIFICATE-----')
        end
      end
    end
    @secrets_with_certs = @secrets_with_certs.stringify_keys
  end

  def update(path, data)
    Logging.debug "Updating path '#{@mount}/#{path}'"

    res = Vault.kv(@mount).write(path, data)
    Logging.debug "Updated '#{@mount}/#{path}' - new version: #{res.data[:version]}"
  end
end
