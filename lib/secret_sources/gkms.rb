class GKMS
  def initialize(environment, path)
    @environment = environment
    @path        = path
  end

  def decrypt
    output = run_cmd(%W(gcloud --project #{@environment.project} kms decrypt --location global --keyring=gitlab-secrets --key #{@environment.name} --ciphertext-file=#{@path} --plaintext-file=-))
    if json?(output)
      output
    else
      run_cmd(%W(gzip -cdf -), stdin: output)
    end
  end

  def encrypt(content)
    unless ENV['DRY_RUN']
      content = run_cmd(%W(gzip -c -), stdin: content) if use_compression?
      run_cmd(%W(gcloud --project #{@environment.project} kms encrypt --location global --keyring=gitlab-secrets --key #{@environment.name} --ciphertext-file=#{@path} --plaintext-file=-), stdin: content)
    end
  end
end

