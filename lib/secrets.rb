class Secrets
  attr_reader :content

  def initialize(raw_secrets, identifier)
    @content = raw_secrets.is_a?(String) ? JSON.parse(raw_secrets) : raw_secrets
    @updated = []
    @identifier = identifier
  end

  def update_certificates
    update_certificates_if_possible(content)
    @updated
  end

  private

  def update_certificates_if_possible(secrets, json_path = [])
    secrets.each do |k, v|
      if v.is_a?(Hash)
        json_path << k

        update_certificates_if_possible(v, json_path)

        json_path.pop
        next
      elsif !v.is_a?(String)
        next
      end

      next unless v.start_with?('-----BEGIN CERTIFICATE-----')

      prefix = "#{@identifier} / #{(json_path.dup + [k]).join('.')}"

      cert = Certificate.new(v)
      Logging.debug "#{prefix} :: found a certificate for #{cert.cn}"

      unless cert.expiring?
        Logging.debug "#{prefix} :: certificate for #{cert.cn} is not close to expiry (expires on #{cert.expiry_date}). Skipping ..."
        next
      end

      if ENV['DRY_RUN']
        Logging.info "#{prefix} :: certificate for #{cert.cn} is close to expiry (expires on #{cert.expiry_date}), but we're running in dry-run mode."
        @updated << [cert.cn, (json_path.dup + [k])]
        next
      end

      Logging.info "#{prefix} :: certificate for #{cert.cn} is close to expiry (expires on #{cert.expiry_date}). Renewing."

      begin
        new_cert = SSLMate.fetch_certificate(cert.cn)
      rescue SSLMate::CertificateNotFound
        Logging.error "#{prefix} :: no certificate found for '#{cert.cn}' on SSLMate"
        next
      rescue SSLMate::RequestError => e
        Logging.error "#{prefix} :: fetching a certificate for '#{cert.cn}' failed. Error: #{e}"
        next
      rescue => e
        Logging.error "#{prefix} :: unexpected error: #{e}"
        next
      end

      unless valid_certificate?(new_cert)
        Logging.error "#{prefix} :: new certificate for '#{cert.cn}' is invalid, Skipping ..."
        next
      end

      Logging.debug "#{prefix} :: valid certificate for '#{cert.cn}' was found on SSLMate"

      key_for_pk = %w(key private_key pk)
        .map { |key| k.gsub(/ce?rt(ificate)?/, key) }
        .find { |key| secrets.key?(key) }
      pk = secrets[key_for_pk]

      if pk && !valid_certificate_key_pair?(new_cert, pk)
        Logging.error "#{prefix} :: matching private key with #{cert.cn} failed! Skipping ..."
        next
      end

      secrets[k] = new_cert
      @updated   << [cert.cn, (json_path.dup + [k])]

      Logging.info "#{prefix} :: certificate for #{cert.cn} has been updated"
    end

    def valid_certificate?(cert)
      !Certificate.new(cert).expiring?
    rescue OpenSSL::X509::CertificateError
      false
    end

    def valid_certificate_key_pair?(cert, key)
      cert_obj = Certificate.new(cert)
      cert_obj.same_modulus_as_key?(OpenSSL::PKey.read(key))
    rescue => e
      Logging.error "Encountered the following error while verifying #{cert_obj.cn} with its private key:"
      Logging.error e.to_s
      Logging.error e.backtrace.join("\n")

      false
    end
  end
end
