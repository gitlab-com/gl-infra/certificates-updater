class Environment
  ENVIRONMENTS = {
    pre: 'gitlab-pre',
    ops: 'gitlab-ops',
    gstg: 'gitlab-staging-1',
    gprd: 'gitlab-production',
    release: 'gitlab-release',
  }

  attr_reader :name, :project, :prefix

  def self.all
    ENVIRONMENTS.map { |(name, project)| new(name, project) }
  end

  def initialize(name, project)
    @name    = name
    @project = project
    @prefix  = "env/#{@name}"
  end

  def download_vaults(to:)
    Logging.info "Downloading secrets from gs://gitlab-#{@name}-secrets to #{to}"
    args = %W(gsutil -m)
    args << '-q' unless Logging.logger.level == Logger::DEBUG
    args << 'cp' << '-r'
    args << "gs://gitlab-#{@name}-secrets"
    args << to
    run_cmd(args)
  end

  def upload_vaults(from:)
    Logging.info "Uploading secrets from '#{from}/gitlab-#{@name}-secrets/' to 'gs://gitlab-#{@name}-secrets/'" unless ENV['DRY_RUN']
    args = %W(gsutil -m)
    args << '-q' unless Logging.logger.level == Logger::DEBUG
    args << 'rsync' << '-r'
    args << '-n' if ENV['DRY_RUN']
    args << "#{from}/gitlab-#{@name}-secrets/"
    args << "gs://gitlab-#{@name}-secrets/"
    run_cmd(args)
  end
end
