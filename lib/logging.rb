require 'logger'

module Logging
  def self.logger
    if @logger.nil?
      @logger = Logger.new(STDOUT)
      @logger.datetime_format = "%H:%M:%S "

      original_formatter = Logger::Formatter.new
      @logger.formatter = proc do |severity, datetime, progname, msg|
        if ENV['TRACE']
          fileLine = "";
          caller.each do |clr|
              unless(/\/(logger|logging).rb:/ =~ clr)
                  fileLine = clr;
                  break;
              end
          end
          fileLine = fileLine.split(':in `',2)[0];
          original_formatter.call(severity, datetime, "#{progname} #{fileLine}", msg)
        else
          original_formatter.call(severity, datetime, progname, msg)
        end
      end
    end
    @logger
  end

  def self.logger=(logger)
    @logger = logger
  end

  levels = %w(debug info warn error fatal)
  levels.each do |level|
    define_method("#{level.to_sym}") do |msg, new_line = false|
      puts if Logging.logger.level <= Logger.const_get(level.upcase) && new_line

      self.logger.send(level, msg)
    end
  end
end

include Logging
