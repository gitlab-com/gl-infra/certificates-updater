class Certificate
  ONE_WEEK = 7 * 24 * 60 * 60

  def initialize(content)
    @cert = OpenSSL::X509::Certificate.new(content)
  end

  def expiring?
    (expiry_date - Time.now.utc) < (2 * ONE_WEEK)
  end

  def expiry_date
    @cert.not_after
  end

  def cn
    cn_entry = @cert.subject.to_a.find { |(oid, _, _)| oid == 'CN' }
    return unless cn_entry

    cn_entry[1]
  end

  def same_modulus_as_key?(key)
    case key
    when OpenSSL::PKey::RSA
      @cert.public_key.n.to_s(16) == key.n.to_s(16)
    when OpenSSL::PKey::EC
      @cert.public_key.public_key.to_bn.to_s(16) == key.public_key.to_bn.to_s(16)
    else
      false
    end
  end
end
