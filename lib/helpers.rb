def json?(input)
  JSON.parse(input)
  true
rescue JSON::ParserError
  false
end

def use_compression?
  ENV['USE_COMPRESSION'] == 'true'
end

def run_cmd(args, allow_failure: false, stdin: nil)
  output = nil

  Logging.debug "Running command: #{args}"
  IO.popen(args, 'r+') do |cmd_io|
    if stdin
      cmd_io.write(stdin)
      cmd_io.close_write
    end

    output = cmd_io.read
  end

  exit_status = $?.exitstatus
  if !allow_failure && exit_status != 0
    Logging.error "Command execution failed. Exit status = #{exit_status}"
    exit 1
  end

  output
end

def notify_via_slack(changes)
  return unless ENV['SLACK_WEBHOOK_URL'] && ENV['SLACK_CHANNEL']
  return if changes.empty?

  msg = "The following certificates were automatically renewed:\n"

  changes.each do |(environment, changes_per_vault)|
    msg << "* #{environment.name}\n"

    changes_per_vault.each do |(vault_path, cns)|
      msg << "  * #{vault_path}\n"

      cns.each do |(cn, json_path)|
        msg << "    * `#{cn}` (JSON: `#{json_path.join('.')}`)\n"
      end
    end
  end

  msg << "CI: #{ENV['CI_JOB_URL']}" if ENV['CI_JOB_URL']

  send_slack_msg(msg)
end

def send_slack_msg(msg)
  if ENV['DRY_RUN']
    puts "[DRY-RUN] The following message would have been sent to Slack:\n\n"
    puts msg

    return
  end

  uri = URI(ENV['SLACK_WEBHOOK_URL'])
  req = Net::HTTP::Post.new(uri)

  payload = {
    channel: ENV['SLACK_CHANNEL'],
    text: msg,
    mrkdwn: true
  }

  req.set_form_data('payload' => JSON.generate(payload))

  Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
    http.request(req)
  end
end

def get_required_env(var)
  value = ENV[var]

  unless value
    Logging.error "Environment variable '#{var}' is not set."
    exit 1
  end

  value
end

class ::Hash
  def stringify_keys
    h = map do |k, v|
      v_str = if v.instance_of? Hash
                v.stringify_keys
              else
                v
              end

      [k.to_s, v_str]
    end
    Hash[h]
  end
end
