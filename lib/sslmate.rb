class SSLMate
  RequestError        = Class.new(StandardError)
  CertificateNotFound = Class.new(StandardError)

  @@endpoint = ENV['SSLMATE_API'] || "https://sslmate.com"

  class << self
    def fetch_certificate(cn)
      uri = URI("#{@@endpoint}/api/v2/certs/#{cn}?expand=current.crt&expand=current.chain")

      req = Net::HTTP::Get.new(uri)
      req.basic_auth(get_required_env('SSLMATE_API_KEY'), '')

      res  = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) { |http| http.request(req) }
      raise RequestError.new("Unsuccessful request (code = #{res.code}). Response was: #{res.body}") if res.code != '200'

      body = JSON.parse(res.body)
      raise CertificateNotFound.new unless body['exists'] && body['current']

      body['current']['crt'] + body['current']['chain'].join
    end
  end
end
