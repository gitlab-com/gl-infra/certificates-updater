# Certificates Updater

A script that updates soon-to-be-expired SSLMate certificates in GKMS and Vault.

The GCP service account (`certificate-updater@gitlab-ops.iam.gserviceaccount.com`)
expects "Storage Object Admin" (for the secrets buckets) and "Cloud KMS CryptoKey Encrypter/Decrypter"
permissions.

The following environment variables are expected by the CI pipeline:
* `SSLMATE_API_KEY`: API key obtained from SSLMate admin panel
* `GCE_KEY`: Base64-encoded string of the GCP service account key
* `ENVIRONMENTS`: Optional, a comma-separated list of environments to target. If empty, all environments are targeted.
* `DRY_RUN`: Optional, makes the script do everything except for uploading anything back to the secrets bucket
* `OTHER_VAULT_PATHS`: Optional, a comma-separated list of Vault paths to include in the scan, if they're not under `chef/*`. (Ensure this project has access to those paths first!)
* `SLACK_WEBHOOK_URL`: Optional, send a message to a Slack channel about which certificates were updated
* `SLACK_CHANNEL`: Required if `SLACK_WEBHOOK_URL` is set, sets the channel to which the notification is sent

## Local development

To connect to Vault, you'll need to port forward to the Vault server using `kubectl` and ensure `VAULT_SSL_VERIFY` is `false`.